/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_malloc.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/12 17:17:58 by avisuissa         #+#    #+#             */
/*   Updated: 2022/01/17 04:22:03 by asuissa          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MALLOC_H
# define FT_MALLOC_H

# include <stdarg.h>
# include <wchar.h>
# include <sys/mman.h>
# include <stdio.h>
# include <unistd.h>
# include <string.h>
# include <stddef.h>
# include <pthread.h>

/*
** int min/max
*/

# define INT_MAX 2147483647
# define INT_MIN -2147483648

# define ERROR 0
# define SUCCESS 1

# define BUFF_SIZE 5000

# define NB_ZONE 100

/*
** Code Couleur
*/

# define BLACK "\033[30m"
# define RED "\033[31m"
# define GREEN "\033[32m"
# define YELLOW "\033[33m"
# define BLUE "\033[34m"
# define PURPLE "\033[35m"
# define EOC "\033[0m"
# define CLEAR "\e[1;1H\e[2J"

/*
** Structures
*/

typedef struct s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}					t_list;

typedef struct s_mmaloc
{
	size_t			size;
	void			*ptr;
	struct s_mmaloc	*next;
}					t_mmaloc;

typedef struct s_size_part // a modifier en t_mmaloc
{
	t_mmaloc		*ptr;
	unsigned int	nb_used;
	void			*start;
	void			*end;
}					t_size_part;

typedef struct s_size_mem
{
	t_size_part		*start_tiny;
	t_size_part		*start_small;
	t_mmaloc		g_malloc_main;
}					t_size_mem;

/*
** libft
*/

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
char				*ft_strrev(char *str);
size_t				ft_strlen(const char *s);
char				*ft_itoa(long long n);
void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putnbr(long n);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);
size_t				ft_strclen(const char *s, char c);
char				*ft_strchr(const char *s, int c);
char				*ft_strnew(size_t size);
char				*ft_strncat(char *s1, const char *s2, size_t n);
void				*ft_memalloc(size_t size);
int					ft_strnchr(const char *s, int c);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_itoa_base(long long nb, char *base);
long				ft_atoi_base(char *str, char *base);
long				ft_atoi(const char *str);

/*
** ft_printf mini
*/

int					ft_printf(const char *format, ...);
char				*del_pourcent(char *str);
int					nb_pourcent(char *str);
int					real_job(char *str, va_list *ap);
int					ft_wcharlen(wchar_t wchar);
size_t				ft_wbytelen(wchar_t *ws);
size_t				ft_wstrlen(wchar_t *ws);
int					ft_putwchar_in_char(wchar_t wchar, char *fresh, int i);
char				*ft_transform_wchar(wchar_t *ws);

int					spec_w(va_list *ap, char *out);
int					spec_y(va_list *ap, char *out);
int					spec_b(va_list *ap, char *out);
int					spec_gx(va_list *ap, char *out);
int					spec_x(va_list *ap, char *out);
int					spec_u(va_list *ap, char *out);
int					spec_o(va_list *ap, char *out);
int					spec_c(va_list *ap, char *out);
int					spec_d(va_list *ap, char *out);
int					spec_p(va_list *ap, char *out);
int					spec_s(va_list *ap, char *out);
int					spec_gc(va_list *ap, char *out);
int					spec_gs(va_list *ap, char *out);

/*
** ft_malloc
*/

t_size_part         *init_tiny(void);
t_size_part         *init_small(void);
void                *ft_malloc(size_t size);
void                *ft_realloc(void *ptr, size_t size);
void                *alloc_mem(size_t size);
void                *select_mem_tiny(size_t size);
void                *select_mem_small(size_t size);
void                *alloc_mem(size_t size);
void                show_alloc_mem(void);
void                ft_free(void *ptr);
void				*ft_calloc(size_t count, size_t size);
void                free_mem(void *ptr);
void    			*malloc(size_t size);
void    			*realloc(void *ptr, size_t size);
void    			free(void *ptr);
void    			*calloc(size_t count, size_t size);
int 				is_in_large(void *ptr);

void free_all (void) __attribute__((destructor));

extern t_size_mem	g_start_size_chain;
extern pthread_mutex_t mutex_stock;
#endif /* FT_MALLOC_H */