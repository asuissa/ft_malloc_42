/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pthread_safe.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 23:57:13 by avisuissa         #+#    #+#             */
/*   Updated: 2022/01/17 00:45:48 by asuissa          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"
#include <pthread.h>
#include <stddef.h>

pthread_mutex_t mutex_stock = PTHREAD_MUTEX_INITIALIZER;

void    *malloc(size_t size)
{
    void    *out = NULL;

    pthread_mutex_lock(&mutex_stock);
    out = alloc_mem(size);
    pthread_mutex_unlock(&mutex_stock);
    return (out);
}

void    *realloc(void *ptr, size_t size)
{
    void    *out = NULL;

    pthread_mutex_lock(&mutex_stock);
    out = ft_realloc(ptr, size);
    pthread_mutex_unlock(&mutex_stock);
    return (out);
}

void    free(void *ptr)
{
    pthread_mutex_lock(&mutex_stock);
    free_mem(ptr);
    pthread_mutex_unlock(&mutex_stock);
}

void    *calloc(size_t count, size_t size)
{
    void    *out = NULL;

    pthread_mutex_lock(&mutex_stock);
    out = ft_calloc(count, size);
    pthread_mutex_unlock(&mutex_stock);
    return (out);
}