/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 17:24:46 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/17 04:25:15 by asuissa          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"

__attribute__((destructor)) void free_all (void)
{
 	if (g_start_size_chain.start_tiny)
	{
			ft_free(g_start_size_chain.start_tiny->start);
			g_start_size_chain.start_tiny->start = NULL;
			ft_free(g_start_size_chain.start_tiny->ptr);
			g_start_size_chain.start_tiny->ptr = NULL;
			ft_free(g_start_size_chain.start_tiny);
			g_start_size_chain.start_tiny = NULL;
	}
	if (g_start_size_chain.start_small)
	{
			ft_free(g_start_size_chain.start_small->start);
			g_start_size_chain.start_small->start = NULL;
			ft_free(g_start_size_chain.start_small->ptr);
			g_start_size_chain.start_small->ptr = NULL;
			ft_free(g_start_size_chain.start_small);
			g_start_size_chain.start_small = NULL;
	}
}

int 	is_in_large(void *ptr)
{
	t_mmaloc	*tmp = NULL;

	tmp = g_start_size_chain.g_malloc_main.next;
	while (tmp && tmp->ptr != ptr)
		tmp = tmp->next;
	if (!tmp)
		return (0);
	return (1);
}

void	ft_free(void *ptr)
{
	size_t		size;
	t_mmaloc	*tmp = NULL;
	t_mmaloc	*tmp2 = NULL;

	if (!ptr)
		return ;
	tmp = g_start_size_chain.g_malloc_main.next;
	while (tmp && tmp->next && tmp->next->ptr != ptr)
		tmp = tmp->next;
	if (!tmp || (!tmp->next && g_start_size_chain.g_malloc_main.next->ptr != ptr))
		return ;
	size = (g_start_size_chain.g_malloc_main.next->ptr == ptr) ?
			g_start_size_chain.g_malloc_main.next->size : tmp->next->size;
	tmp2 = (g_start_size_chain.g_malloc_main.next->ptr == ptr) ?
			g_start_size_chain.g_malloc_main.next->next : tmp->next->next;
	if (!g_start_size_chain.g_malloc_main.next->next)
	{
		munmap(g_start_size_chain.g_malloc_main.next, sizeof(t_mmaloc) + size);
		g_start_size_chain.g_malloc_main.next = NULL;
	}
	else
	{
		munmap(tmp->next, sizeof(t_mmaloc) + size);

        if (g_start_size_chain.g_malloc_main.next && g_start_size_chain.g_malloc_main.next->ptr == ptr)
            g_start_size_chain.g_malloc_main.next = tmp2;
        else
            tmp->next = tmp2;
	}
}

//a rename en free
void	free_mem(void *ptr)
{
	t_mmaloc	*tmp;
	int			i;

	if (g_start_size_chain.start_tiny && ptr >= g_start_size_chain.start_tiny->start && ptr <= g_start_size_chain.start_tiny->end && !is_in_large(ptr)) // laissez les zone memoire
	{
		tmp = g_start_size_chain.start_tiny->ptr;
		i = 0;
		while (i < NB_ZONE && ptr != tmp[i].ptr)
			i++;
		if (i >= NB_ZONE)
			return ;
		ft_bzero(tmp[i].ptr, tmp[i].size);
		tmp[i].size = 0;
		g_start_size_chain.start_tiny->nb_used -= 1;
		return ;
	}
	else if (g_start_size_chain.start_small && ptr >= g_start_size_chain.start_small->start && ptr <= g_start_size_chain.start_small->end && !is_in_large(ptr)) // laissez les zone memoire
	{
		tmp = g_start_size_chain.start_small->ptr;
		i = 0;
		while (i < NB_ZONE && ptr != tmp[i].ptr)
			i++;
		if (i >= NB_ZONE)
			return ;
		ft_bzero(tmp[i].ptr, tmp[i].size);
		tmp[i].size = 0;
		g_start_size_chain.start_small->nb_used -= 1;
		return ;
	}
	else
		ft_free(ptr);
}