/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tiny.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 17:27:55 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/17 03:41:43 by asuissa          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"

t_size_part	*init_tiny(void)
{
	size_t	syspage;
	t_size_part	*out;
	t_mmaloc	*tmp;
	int			i;

    syspage = getpagesize();
	if (!(out = (t_size_part*)ft_malloc(sizeof(t_size_part) + (sizeof(t_mmaloc) * NB_ZONE) + (syspage * NB_ZONE))))
		return (NULL);
    tmp = (t_mmaloc*)(out + 1);
    tmp[0].ptr = (tmp + NB_ZONE);
	i = 1;
	while (i < NB_ZONE)
	{
		tmp[i].ptr = (tmp[i - 1].ptr + syspage);
		tmp[i].size = 0;
		i++;
	}
	ft_bzero(out, sizeof(t_size_part));
	out->nb_used = 0;
	out->ptr = tmp;
	out->start = tmp[0].ptr;
	out->end = tmp[99].ptr;
	return (out);
}

void *select_mem_tiny(size_t size)
{
	t_mmaloc		*tmp;
	int				i;

	tmp = g_start_size_chain.start_tiny->ptr;
	if (!tmp)
		return (NULL);
	if (g_start_size_chain.start_tiny->nb_used >= NB_ZONE)
		return (ft_malloc(size));
	i = 0;
	while (i < NB_ZONE)
	{
		if ((tmp[i]).size == 0)
		{
			(tmp[i]).size = size;
			return ((tmp[i]).ptr);
		}
		i++;
	}
	return (NULL);
}