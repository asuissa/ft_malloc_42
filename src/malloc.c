/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 17:23:47 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/17 02:51:07 by asuissa          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"
#include <stddef.h>
#include <stdint.h>

t_size_mem			g_start_size_chain = {0};

int is_in_tiny(void *ptr)
{
	t_mmaloc		*tmp;
	int				i;

	tmp = g_start_size_chain.start_tiny->ptr;
	if (!tmp)
		return (0);
	i = 0;
	while (i < NB_ZONE)
	{
		if ((tmp[i]).ptr == ptr)
			return (1);
		i++;
	}
	return (0);
}

int is_in_small(void *ptr)
{
	t_mmaloc		*tmp;
	int				i;

	tmp = g_start_size_chain.start_small->ptr;
	if (!tmp)
		return (0);
	i = 0;
	while (i < NB_ZONE)
	{
		if ((tmp[i]).ptr == ptr)
			return (1);
		i++;
	}
	return (0);
}

void	*ft_malloc(size_t size)
{
	t_mmaloc	*struct_test;
	t_mmaloc	*tmp;

	tmp = g_start_size_chain.g_malloc_main.next;
	struct_test = (t_mmaloc*)mmap(NULL, (size + sizeof(t_mmaloc)), PROT_WRITE | PROT_READ, MAP_PRIVATE
			| MAP_ANON, -1, 0);
    if (struct_test == MAP_FAILED)
		return (NULL);
	ft_bzero(struct_test, sizeof(t_mmaloc));
    struct_test->ptr = struct_test + 1;
	struct_test->size = size;
	if (!tmp)
		g_start_size_chain.g_malloc_main.next = struct_test;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = struct_test;
	}
	return (struct_test->ptr);
}

void    *ft_realloc(void *ptr, size_t size)
{
	t_mmaloc	*tmp = NULL;
	void		*ptr_tmp = NULL;
	size_t		syspage;

	syspage = getpagesize();
	if (!ptr)
		return (NULL);
	if (g_start_size_chain.start_tiny && (ptr >= g_start_size_chain.start_tiny->start && ptr <= g_start_size_chain.start_tiny->end) && !is_in_large(ptr))
	{
		if (!is_in_tiny(ptr))
		{
			free_mem(ptr);
			return (NULL);
		}
		if ((ptr_tmp = alloc_mem(size)))
			ptr_tmp = ft_memmove(ptr_tmp, ptr, size > syspage ? syspage : size);
	}
	else if (g_start_size_chain.start_small && (ptr >= g_start_size_chain.start_small->start && ptr <= g_start_size_chain.start_small->end) && !is_in_large(ptr))
	{
		if (!is_in_small(ptr))
		{
			free_mem(ptr);
			return (NULL);
		}
		if ((ptr_tmp = alloc_mem(size)))
			ptr_tmp = ft_memmove(ptr_tmp, ptr, size > syspage * 2 ? syspage * 2 : size);
	}
	else
	{
		tmp = g_start_size_chain.g_malloc_main.next;
		while (tmp && tmp->ptr != ptr)
			tmp = tmp->next;
		if (!tmp)
		{
			free_mem(ptr);
			return (NULL);
		}
		if ((ptr_tmp = alloc_mem(size)))
			ptr_tmp = ft_memmove(ptr_tmp, ptr, size > tmp->size ? tmp->size : size);
	}
	free_mem(ptr); //position importante !!
	return (ptr_tmp);
}

void	*ft_calloc(size_t count, size_t size)
{
	void *out = NULL;

	if (!count || !size || ((count * size) > SIZE_MAX))
		return (NULL);
	out = alloc_mem(size * count);
	ft_bzero(out, size * count);
	return (out);
}

//a rename en malloc
void	*alloc_mem(size_t size)
{
	size_t		syspage;
	void		*out = NULL;

	syspage = getpagesize();
	if (!size)
		return (NULL);
	if (size <= syspage)
	{
		if (!g_start_size_chain.start_tiny)
			if (!(g_start_size_chain.start_tiny = init_tiny()))
				return (NULL);
		if (!(out = select_mem_tiny(size)))
		{
			printf("fail\n");
			return (NULL);
		}
		g_start_size_chain.start_tiny->nb_used += 1;
	}
	else if (size <= (syspage * 2))
	{
		if (!g_start_size_chain.start_small)
			if (!(g_start_size_chain.start_small = init_small()))
				return (NULL);
		if (!(out = select_mem_small(size)))
			return (NULL);
		g_start_size_chain.start_small->nb_used += 1;
	}
	else
		out = ft_malloc(size);
	return (out);
}

void	show_alloc_mem(void)
{
	size_t	total_size;
	int		i;
	t_mmaloc *tmp;

	total_size = 0;
	ft_printf("DYNAMIC MEMORY USAGE :\n");
	if (g_start_size_chain.start_tiny && g_start_size_chain.start_tiny->nb_used > 0)
	{
		ft_printf("TINY : %p\n", g_start_size_chain.start_tiny->ptr);
		i = -1;
		tmp = g_start_size_chain.start_tiny->ptr;
		while (++i < NB_ZONE)
		{
			if (tmp[i].size > 0)
			{
				ft_printf("%p - %p : %i octets\n", tmp[i].ptr, (tmp[i].ptr + tmp[i].size), tmp[i].size);
				total_size += tmp[i].size;
			}
		}
	}
	if (g_start_size_chain.start_small && g_start_size_chain.start_small->nb_used > 0)
	{
		ft_printf("SMALL : %p\n", g_start_size_chain.start_small->ptr);
		i = -1;
		tmp = g_start_size_chain.start_small->ptr;
		while (++i < NB_ZONE)
		{
			if (tmp[i].size > 0)
			{
				ft_printf("%p - %p : %i octets\n", tmp[i].ptr, (tmp[i].ptr + tmp[i].size), tmp[i].size);
				total_size += tmp[i].size;
			}
		}
	}
	if (g_start_size_chain.g_malloc_main.next)
	{
        ft_printf("LARGE : %p", g_start_size_chain.g_malloc_main.next);
		tmp = g_start_size_chain.g_malloc_main.next;
		i = 0;
		while (tmp)
		{
			if (!((void*)tmp->ptr == (void*)g_start_size_chain.start_tiny) && !((void*)tmp->ptr == (void*)g_start_size_chain.start_small))
            {
				if (!i)
					ft_printf("\n");
                ft_printf("%p - %p : %d octets\n", tmp->ptr, (tmp->ptr + tmp->size), tmp->size);
			    total_size += tmp->size;
				i++;
            }
			tmp = tmp->next;
		}
	}
	ft_printf("\r\e[KTotal : %i octets\n", total_size);
}