/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avisuissa <avisuissa@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/16 16:07:04 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/12 20:23:34 by avisuissa        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"

char	*ft_strnew(size_t size)
{
	char	*out;

	out = (char *)malloc(sizeof(char) * size + 1);
	if (!out)
		return (NULL);
	ft_bzero(out, size + 1);
	return (out);
}
