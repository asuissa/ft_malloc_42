/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   small.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/13 17:28:58 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/17 02:29:26 by asuissa          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"

t_size_part	*init_small(void)
{
	size_t	syspage2;
	t_size_part	*out;
	t_mmaloc	*tmp;
	int			i;

    syspage2 = getpagesize() * 2;
	if (!(out = (t_size_part*)ft_malloc(sizeof(t_size_part) + sizeof(t_mmaloc) * NB_ZONE + syspage2 * NB_ZONE)))
		return (NULL);
	ft_bzero(out, sizeof(t_size_part));
    tmp = (t_mmaloc*)(out + 1);
    tmp[0].ptr = (tmp + NB_ZONE);
    i = 1;
	while (i < NB_ZONE)
	{
		(tmp[i]).ptr = (tmp[i - 1].ptr + syspage2);
		(tmp[i]).size = 0;
		i++;
	}
	out->nb_used = 0;
	out->ptr = tmp;
	out->start = tmp[0].ptr;
	out->end = tmp[99].ptr;
	return (out);
}

void *select_mem_small(size_t size)
{
	t_mmaloc		*tmp;
	int				i;

	tmp = g_start_size_chain.start_small->ptr;
	if (!tmp)
		return (NULL);
	if (g_start_size_chain.start_small->nb_used >= NB_ZONE)
		return (ft_malloc(size));
	i = 0;
	while (i < NB_ZONE)
	{
		if ((tmp[i]).size == 0)
		{
			(tmp[i]).size = size;
			return ((tmp[i]).ptr);
		}
		i++;
	}
	return (NULL);
}