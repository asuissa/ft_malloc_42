/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avisuissa <avisuissa@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/16 15:48:51 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/12 20:22:52 by avisuissa        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"

void	*ft_memalloc(size_t size)
{
	void	*out;

	out = malloc(size);
	if (!out)
		return (NULL);
	ft_bzero(out, size);
	return (out);
}
