/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avisuissa <avisuissa@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/11 20:29:48 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/12 18:13:48 by avisuissa        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"

char	*ft_strrev(char *str)
{
	int		a;
	int		b;
	char	c;

	if (!str)
		return (NULL);
	b = ft_strlen(str) - 1;
	a = 0;
	while (a < b)
	{
		c = str[a];
		str[a] = str[b];
		str[b] = c;
		a++;
		b--;
	}
	return (str);
}
