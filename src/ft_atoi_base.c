/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avisuissa <avisuissa@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/24 21:43:25 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/12 18:10:27 by avisuissa        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/ft_malloc.h"

long		ft_atoi_base(char *str, char *base)
{
	int			i;
	long long	val;
	int			n;

	if (!str)
		return (0);
	val = 0;
	i = 0;
	while (str[i] == ' ' || (str[i] >= '\t' && str[i] <= '\r'))
		i++;
	n = i;
	if (str[i] == '+' || str[i] == '-')
		i++;
	while (ft_strchr(base, str[i]) && str[i])
	{
		val = val * ft_strlen(base) + (ft_strclen(base, str[i]));
		i++;
	}
	if (str[n] == '-')
		val = -val;
	return (val);
}
