# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/08 17:36:49 by asuissa           #+#    #+#              #
#    Updated: 2022/01/17 04:02:14 by asuissa          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re
.SUFFIXES:

# couleurs
RED = \033[31m
GREEN = \033[32m
YELLOW = \033[33m
BLUE = \033[34m
PURPLE = \033[35m
RESET = \033[0m
HEAD = \033[H\033[2J

VPATH = src:inc:libft

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

CC = gcc
CFLAGS = -fPIC -Wall -Wextra -g #-Werror
D_FLAG = -g #-fsanitize=address
HFLAGS = #-I $(INC_PATH) -I libft
LFLAGS = -shared
CLIBFT = -L . -lft_malloc -lpthread
SRC_PATH = src/
INC_PATH = inc/
NAME = libft_malloc_$(HOSTTYPE).so
D_NAME = ft_malloc_debug
OBJ = $(addprefix $(OBJS_DIR), $(FILES:.c=.o))
OBJS_DIR = objs/
INCLUDE = $(addprefix $(INC_PATH), $(HEADER))
SRC = $(addprefix $(SRC_PATH), $(FILES))
HEADER = ft_malloc.h
FILES = \
		ft_atoi.c \
		ft_bzero.c \
		ft_memcmp.c \
		ft_memccpy.c \
		ft_memcpy.c \
		ft_memset.c \
		ft_memmove.c \
		ft_memalloc.c \
		ft_strlen.c \
		ft_strnew.c \
		ft_strcat.c \
		ft_strchr.c \
		ft_strclen.c \
		ft_strncat.c \
		ft_strnchr.c \
		ft_strrev.c \
		ft_itoa.c \
		ft_putchar.c \
		ft_putstr.c \
		ft_putnbr.c \
		ft_putchar_fd.c \
		ft_putstr_fd.c \
		ft_putnbr_fd.c \
		ft_lstnew.c \
		ft_putnbr_base.c \
		ft_atoi_base.c \
		ft_itoa_base.c \
		ft_printf.c \
		specifier.c \
		specifier2.c \
		specifier3.c \
		ft_transform_wchar.c \
		ft_atoi_int.c \
		malloc.c \
		free.c \
		tiny.c \
		small.c \
		pthread_safe.c

all : create_obj_dir $(NAME) libft_malloc.so

$(NAME) : $(OBJ)
	@printf "\r\e[K[$(YELLOW)Make .o done$(RESET)]\n"
	@$(CC) $(LFLAGS) $^ -o $@
	@echo "[$(PURPLE)Make $(NAME) done$(RESET)]"

libft_malloc.so :
	@ln -s $(NAME) $@
	@echo "[$(PURPLE)ln $@ done$(RESET)]"

create_obj_dir:
	@mkdir -p $(OBJS_DIR)

$(OBJ) : $(INCLUDE)

$(OBJS_DIR)%.o: $(SRC_PATH)%.c
	@$(CC) -c $(CFLAGS) $(HFLAGS) $< -o $@
	@printf "\r\e[K[$(YELLOW)Make $@ done$(RESET)]"

clean :
	@/bin/rm -rf $(OBJS_DIR)
	@echo "[$(RED)Clean .o done$(RESET)]"

fclean : clean
	@/bin/rm -f $(NAME)
	@/bin/rm -f libft_malloc.so
	@echo "[$(RED)Clean $(NAME) done$(RESET)]"

re : fclean all

debug :
	@echo "[$(RED)/!\ DEBUG MODE ON $(NAME)$(RESET)]"
	@printf "\r\e[K[$(YELLOW)Make .o done$(RESET)]\n"
	$(CC) $(CFLAGS) $(HFLAGS) main_test_mmap.c $(CLIBFT) $(D_FLAG) -o $(D_NAME)
	@echo "[$(PURPLE)Make $(D_NAME) done$(RESET)]"

debug_clean : clean
	@/bin/rm -f $(D_NAME)
	@/bin/rm -rf $(D_NAME).dSYM
	@echo "[$(RED)Clean $(D_NAME) done$(RESET)]"

debug_re : debug_clean debug
