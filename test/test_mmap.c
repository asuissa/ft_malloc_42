/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_mmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 04:32:59 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/13 17:15:57 by asuissa          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../header/ft_malloc.h"

t_mmaloc	*init_struct(void)
{
	t_mmaloc	*ptr;

	ptr = (t_mmaloc *)mmap(NULL, sizeof(t_mmaloc), PROT_WRITE | PROT_READ,
			MAP_PRIVATE | MAP_ANON, -1, 0);
	if (ptr == MAP_FAILED)
		return (NULL);
	ft_bzero(ptr, sizeof(t_mmaloc));
	return (ptr);
}

void	*ft_malloc(size_t size)
{
	t_mmaloc	*struct_test;
	t_mmaloc	*tmp;

	if (!(struct_test = init_struct()))
		return (NULL);
	tmp = g_start_size_chain.g_malloc_main.next;
	struct_test->ptr = mmap(NULL, size, PROT_WRITE | PROT_READ, MAP_PRIVATE
			| MAP_ANON, -1, 0);
	if (struct_test->ptr == MAP_FAILED)
		return (NULL);
	struct_test->size = size;
	if (!tmp)
		g_start_size_chain.g_malloc_main.next = struct_test;
	else
	{
		while (tmp->next)
			tmp = tmp->next;
		tmp->next = struct_test;
	}
	return (struct_test->ptr);
}

void    *ft_realloc(void *ptr, size_t size)
{
	t_mmaloc	*tmp;
	void		*ptr_tmp;

	if (!ptr)
		return (NULL);
	tmp = g_start_size_chain.g_malloc_main.next;
	while (tmp && tmp->ptr != ptr)
		tmp = tmp->next;
	if (!tmp)
	{
		write(2, "Error realloc\n", 13); //a sup ??
		return (NULL);
	}
	if (!(ptr_tmp = alloc_mem(size)))
		return (NULL);
	ptr_tmp = ft_memmove(ptr_tmp, ptr, size > tmp->size ? tmp->size : size);
	free_mem(ptr); //position importante !!
	tmp->size = size;
	tmp->ptr = ptr_tmp;
	return (ptr_tmp);
}

void	ft_free(void *ptr)
{
	size_t		size;
	t_mmaloc	*tmp;
	t_mmaloc	*tmp2;

	if (!ptr)
		return ;
	tmp = g_start_size_chain.g_malloc_main.next;
	while (tmp && tmp->next && tmp->next->ptr != ptr)
		tmp = tmp->next;
	if ((!tmp || !tmp->next) && g_start_size_chain.g_malloc_main.next->ptr != ptr)
	{
		write(2, "Error free\n", 11); //a supp ?
		return ;
	}
	size = (g_start_size_chain.g_malloc_main.next->ptr == ptr) ?
			g_start_size_chain.g_malloc_main.next->size : tmp->next->size;
	tmp2 = (g_start_size_chain.g_malloc_main.next->ptr == ptr) ?
			g_start_size_chain.g_malloc_main.next->next : tmp->next->next;
    munmap(ptr, size);
	if (!g_start_size_chain.g_malloc_main.next->next)
	{
		munmap(g_start_size_chain.g_malloc_main.next, sizeof(t_mmaloc));
		g_start_size_chain.g_malloc_main.next = NULL;
	}
	else
	{
		munmap(tmp->next, sizeof(t_mmaloc));

        if (g_start_size_chain.g_malloc_main.next->ptr == ptr)
            g_start_size_chain.g_malloc_main.next = tmp2;
        else
            tmp->next = tmp2;
	}
}

void	free_mem(void *ptr)
{
	t_mmaloc	*tmp;
	int			i;

	if (g_start_size_chain.start_tiny && ptr >= g_start_size_chain.start_tiny->start && ptr <= g_start_size_chain.start_tiny->end) // verifier si existe !!!!!!
	{
		if (g_start_size_chain.start_tiny->nb_used == 1)
		{
			ft_free(g_start_size_chain.start_tiny->start);
			g_start_size_chain.start_tiny->start = NULL;
			ft_free(g_start_size_chain.start_tiny->ptr);
			g_start_size_chain.start_tiny->ptr = NULL;
			ft_free(g_start_size_chain.start_tiny);
			g_start_size_chain.start_tiny = NULL;
			return ;
		}
		tmp = g_start_size_chain.start_tiny->ptr;
		i = 0;
		while (i < 100 && ptr != tmp[i].ptr)
			i++;
		if (i >= 100)
			return ;
		ft_bzero(tmp[i].ptr, tmp[i].size);
		tmp[i].size = 0;
		g_start_size_chain.start_tiny->nb_used -= 1;
	}
	else if (g_start_size_chain.start_small && ptr >= g_start_size_chain.start_small->start && ptr <= g_start_size_chain.start_small->end) // verifier si existe !!!!!!
	{
		if (g_start_size_chain.start_small->nb_used == 1)
		{
			ft_free(g_start_size_chain.start_small->start);
			g_start_size_chain.start_small->start = NULL;
			ft_free(g_start_size_chain.start_small->ptr);
			g_start_size_chain.start_small->ptr = NULL;
			ft_free(g_start_size_chain.start_small);
			g_start_size_chain.start_small = NULL;
			return ;
		}
		tmp = g_start_size_chain.start_small->ptr;
		i = 0;
		while (i < 100 && ptr != tmp[i].ptr)
			i++;
		if (i >= 100)
			return ;
		ft_bzero(tmp[i].ptr, tmp[i].size);
		tmp[i].size = 0;
		g_start_size_chain.start_small->nb_used -= 1;
	}
	else
		ft_free(ptr);
}

t_size_part	*init_tiny(void)
{
	size_t	syspage;
	t_size_part	*out;
	t_mmaloc	*tmp;
	int			i;

	if (!(out = (t_size_part*)ft_malloc(sizeof(t_size_part))))
		return (NULL);
	ft_bzero(out, sizeof(t_size_part));
	syspage = getpagesize();
	if (!(tmp = (t_mmaloc*)ft_malloc(sizeof(t_mmaloc) * 100)))
		return (NULL);
	if (!(tmp[0].ptr = ft_malloc(syspage * 100 + 1)))
		return (NULL);
	//a virer ?
	ft_bzero(tmp[0].ptr, syspage * 100 + 1);
    i = 1;
	while (i < 100)
	{
		(tmp[i]).ptr = (tmp[i - 1].ptr + syspage);
		(tmp[i]).size = 0;
		i++;
	}
	out->nb_used = 0;
	out->ptr = tmp;
	out->start = tmp[0].ptr;
	out->end = tmp[99].ptr;
	return (out);
}

t_size_part	*init_small(void)
{
	size_t	syspage2;
	t_size_part	*out;
	t_mmaloc	*tmp;
	int			i;

	if (!(out = (t_size_part*)ft_malloc(sizeof(t_size_part))))
		return (NULL);
	ft_bzero(out, sizeof(t_size_part));
	syspage2 = getpagesize() * 2;
	if (!(tmp = (t_mmaloc*)ft_malloc(sizeof(t_mmaloc) * 100)))
		return (NULL);
	if (!(tmp[0].ptr = ft_malloc(syspage2 * 100 + 1)))
		return (NULL);
	//a virer ?
	ft_bzero(tmp[0].ptr, syspage2 * 100 + 1);
    i = 1;
	while (i < 100)
	{
		(tmp[i]).ptr = (tmp[i - 1].ptr + syspage2);
		(tmp[i]).size = 0;
		i++;
	}
	out->nb_used = 0;
	out->ptr = tmp;
	out->start = tmp[0].ptr;
	out->end = tmp[99].ptr;
	return (out);
}

void *select_mem_tiny(size_t size)
{
	t_mmaloc		*tmp;
	int				i;

	tmp = g_start_size_chain.start_tiny->ptr;
	if (!tmp)
		return (NULL);
	if (g_start_size_chain.start_tiny->nb_used >= 100)
		return (ft_malloc(size));
	i = 0;
	while (i < 100)
	{
		if ((tmp[i]).size == 0)
		{
			(tmp[i]).size = size;
			return ((tmp[i]).ptr);
		}
		i++;
	}
	return (NULL);
}

void *select_mem_small(size_t size)
{
	t_mmaloc		*tmp;
	int				i;

	tmp = g_start_size_chain.start_small->ptr;
	if (!tmp)
		return (NULL);
	if (g_start_size_chain.start_small->nb_used >= 100)
		return (ft_malloc(size));
	i = 0;
	while (i < 100)
	{
		if ((tmp[i]).size == 0)
		{
			(tmp[i]).size = size;
			return ((tmp[i]).ptr);
		}
		i++;
	}
	return (NULL);
}

//a rename en malloc
void	*alloc_mem(size_t size)
{
	size_t		syspage;
	void		*out = NULL; //norme a faire

	syspage = getpagesize();
	if (size <= syspage)
	{
		if (!g_start_size_chain.start_tiny)
			if (!(g_start_size_chain.start_tiny = init_tiny()))
				return (NULL);
		if (!(out = select_mem_tiny(size)))
			return (NULL);
		g_start_size_chain.start_tiny->nb_used += 1;
	}
	else if (size <= (syspage * 2))
	{
		if (!g_start_size_chain.start_small)
			if (!(g_start_size_chain.start_small = init_small()))
				return (NULL);
		if (!(out = select_mem_small(size)))
			return (NULL);
		g_start_size_chain.start_small->nb_used += 1;
	}
	else //a travailler
		out = ft_malloc(size);
	return (out);
}

void	show_alloc_mem(void)
{
	size_t	total_size;
	int		i;
	t_mmaloc *tmp;

	total_size = 0;
	ft_printf("DYNAMIC MEMORY USAGE :\n");
	if (g_start_size_chain.start_tiny)
	{
		ft_printf("TINY : %p\n", g_start_size_chain.start_tiny->ptr);
		i = -1;
		tmp = g_start_size_chain.start_tiny->ptr;
		while (++i < 100)
		{
			if (tmp[i].size > 0)
			{
				ft_printf("%p - %p : %i octets\n", tmp[i].ptr, (tmp[i].ptr + tmp[i].size), tmp[i].size);
				total_size += tmp[i].size;
			}
		}
	}
	if (g_start_size_chain.start_small)
	{
		ft_printf("SMALL : %p\n", g_start_size_chain.start_small->ptr);
		i = -1;
		tmp = g_start_size_chain.start_small->ptr;
		while (++i < 100)
		{
			if (tmp[i].size > 0)
			{
				ft_printf("%p - %p : %i octets\n", tmp[i].ptr, (tmp[i].ptr + tmp[i].size), tmp[i].size);
				total_size += tmp[i].size;
			}
		}
	}
	if (g_start_size_chain.g_malloc_main.next) //a travailler
	{
		ft_printf("LARGE : %p\n", g_start_size_chain.g_malloc_main.next);
		tmp = g_start_size_chain.g_malloc_main.next;
		while (tmp)
		{
			ft_printf("%p - %p : %i octets\n", tmp->ptr, (tmp->ptr + tmp->size), tmp->size);
			total_size += tmp->size;
			tmp = tmp->next;
		}
	}
	ft_printf("Total : %i octets\n", total_size);
}