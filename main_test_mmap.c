/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test_mmap.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: asuissa <asuissa@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 04:32:59 by asuissa           #+#    #+#             */
/*   Updated: 2022/01/17 02:52:41 by asuissa          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "inc/ft_malloc.h"

int main(void)
{
    int size = 8;
	size_t	syspage = 0;
    char *c = NULL;
	char *d = NULL;
	char *e = NULL;
	//char *h = NULL;

	syspage = getpagesize();
    /*c = (char*)ft_malloc(size);
    sprintf(c,"bonjour");
	printf("get : %d\nget_sys : %ld\nc : %s\n", getpagesize(), sysconf(_SC_PAGE_SIZE), c);
	d = (char*)ft_malloc(size);
	printf("malloc : c : %s\n", c);
    sprintf(d,"benjoie");
	printf("malloc : d : %s\n", d);
	show_alloc_mem();
	c = ft_realloc(c, 5); //realloc bug !!!!!!!!!!!!!!!!!!!!!!!!!!!
	c[4] = '\0';
	printf("realloc : c : %s\n", c);
	e = (char*)ft_malloc(4);
	sprintf(e,"abc");
	printf("malloc : e : %s\n", e);
    free_mem(c);
	ft_free(d);
	ft_free(e);

	//int i = 0;
	/*while (i < 10)
	{
		c = alloc_mem(i + 10);
		free_mem(c);
		i++;
	}*/
	//c = alloc_mem(syspage); //a partir de 4 tour de boucle ça segfault lecture/ecriture impossible sur c
	//sprintf(c,"a");
	/*d = alloc_mem(syspage);
	sprintf(d,"rtyabcde");
	e = alloc_mem(syspage * 2);
	printf("test medium c : %s\n", c);
	printf("test medium d : %s\n", d);
	//sprintf(c,"abcdefgiy");
	printf("test tiny cd : %s\n", c);
	free_mem(d);
	free_mem(e);*/
	//ft_printf("%i\n", syspage * 3);
	c = malloc(syspage);
	d = malloc(syspage * 2);
	e = calloc(3, syspage);
	show_alloc_mem();
	c = ft_realloc(c, syspage * 2);

	show_alloc_mem();
	c = realloc(c, syspage * 3);
	show_alloc_mem();
	c = ft_realloc(c, syspage);
	show_alloc_mem();
	free(c);
	free(d);
	free(e);
	show_alloc_mem();
	//while (1);*/
    return (0);
}